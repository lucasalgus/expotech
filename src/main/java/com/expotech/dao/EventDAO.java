package com.expotech.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.expotech.base.Event;
import com.expotech.enums.TypeOccupationArea;

public class EventDAO {
	private List<Event> events;
	private File file;
	private FileOutputStream fos;
	private ObjectOutputStream outputFile;

	public EventDAO(String filename) throws IOException {

		events = new ArrayList<Event>();

		file = new File(filename);
		close();
		readFromFile();
	}

	public void add(Event event) {
		Long nextId = getNextId();
		
		if(nextId == 0L) {
			System.out.println("ERRO ao gravar o evento '" + event.getTitle() + "' no disco!");
			return;
		}
		
		event.setId(nextId);
		
		try {
			events.add(event);
			saveToFile();
		} catch (Exception e) {
			System.out.println("ERRO ao gravar o evento '" + event.getTitle() + "' no disco!");
			e.printStackTrace();
		}
	}

	public Event get(Long chave) {
		Event event = null;

		try (FileInputStream fis = new FileInputStream(file);
				ObjectInputStream inputFile = new ObjectInputStream(fis)) {
			while (fis.available() > 0) {
				event = (Event) inputFile.readObject();

				if (chave.equals(event.getId())) {
					return event;
				}
			}
		} catch (Exception e) {
			System.out.println("ERRO ao ler o evento '" + chave + "' do disco!");
			e.printStackTrace();
		}
		return null;
	}

	public void update(Event u) {
		List<Event> eventsFiltered = events.stream().filter(event -> event.getId().equals(u.getId())).collect(Collectors.toList());
		
		if(eventsFiltered.isEmpty()) {
			return;
		}
		
		int index = events.indexOf(eventsFiltered.get(0));
		if (index != -1) {
			events.set(index, u);
			saveToFile();
		}
	}

	public void remove(Event u) {
		List<Event> eventsFiltered = events.stream().filter(event -> event.getId().equals(u.getId())).collect(Collectors.toList());
		
		if(eventsFiltered.isEmpty()) {
			return;
		}
		
		int index = events.indexOf(eventsFiltered.get(0));
		if (index != -1) {
			events.remove(index);
			saveToFile();
		}
	}

	public List<Event> getAll() {
		Date date = new Date();
		return events.stream().filter(e -> date.compareTo(e.getStartDate()) <= 0).collect(Collectors.toList());
	}

	public List<Event> getAll(int organizerId) {
		return events.stream().filter(e -> e.getOrganizer() != null && e.getOrganizer().getId() == organizerId).collect(Collectors.toList());
	}
	
	public List<Event> getAll(TypeOccupationArea type) {
		Date date = new Date();
		return events.stream().filter(e -> type != null && type.equals(e.getType())).filter(e -> date.compareTo(e.getStartDate()) <= 0).collect(Collectors.toList());
	}

	private List<Event> readFromFile() {
		Event Event = null;
		try (FileInputStream fis = new FileInputStream(file);
				ObjectInputStream inputFile = new ObjectInputStream(fis)) {

			while (fis.available() > 0) {
				Event = (Event) inputFile.readObject();
				events.add(Event);
			}
		} catch (Exception e) {
			System.out.println("ERRO ao ler evento no disco!");
			e.printStackTrace();
		}
		return events;
	}

	private void saveToFile() {
		try {
			close();
			fos = new FileOutputStream(file, false);
			outputFile = new ObjectOutputStream(fos);

			for (Event Event : events) {
				outputFile.writeObject(Event);
			}
			outputFile.flush();
		} catch (Exception e) {
			System.out.println("ERRO ao gravar evento no disco!");
			e.printStackTrace();
		}
	}

	private void close() throws IOException {
		if (outputFile != null ) {
			outputFile.close();
			fos.close();
			outputFile = null;
			fos = null;
		}
	}
	
	private Long getNextId() {
		if(events == null) {
			return 0L;
		} else if(events.isEmpty()) {
			return 1L;
		}
		
		return events.get(events.size() - 1).getId() + 1;
	}
	
	public boolean titleExists(final String title) {
		return !events.stream().filter(e -> e.getTitle().equals(title)).collect(Collectors.toList()).isEmpty();
	}

	@Override
	public void finalize() throws Throwable {
		this.close();
	}

}
