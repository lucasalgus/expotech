package com.expotech.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import com.expotech.base.Exhibitor;
import com.expotech.base.Organizer;
import com.expotech.base.User;
import com.expotech.dto.ResponseLoginDTO;
import com.expotech.enums.UserTypeEnum;

public class UserDAO<T extends User> {
	private List<T> users;
	private File file;
	private FileOutputStream fos;
	private ObjectOutputStream outputFile;

	public UserDAO(String filename) throws IOException {

		users = new ArrayList<T>();

		file = new File(filename);
		close();
		readFromFile();
	}

	public void add(T user) {
		Long nextId = getNextId();
		
		if(nextId == 0L) {
			System.out.println("ERRO ao gravar o usuario '" + user.getName() + "' no disco!");
			return;
		}
		
		user.setId(nextId);
		
		try {
			users.add(user);
			saveToFile();
		} catch (Exception e) {
			System.out.println("ERRO ao gravar o usuario '" + user.getName() + "' no disco!");
			e.printStackTrace();
		}
	}

	public T get(Long chave) {
		T user = null;

		try (FileInputStream fis = new FileInputStream(file);
				ObjectInputStream inputFile = new ObjectInputStream(fis)) {
			while (fis.available() > 0) {
				user = (T) inputFile.readObject();
				
				if (chave.equals(user.getId())) {
					return user;
				}
			}
		} catch (Exception e) {
			System.out.println("ERRO ao ler o usuario '" + chave + "' do disco!");
			e.printStackTrace();
		}
		return null;
	}

	public void update(final T u) {
		List<User> usersFiltered = users.stream().filter(user -> user.getId().equals(u.getId())).collect(Collectors.toList());
		
		if(usersFiltered.isEmpty()) {
			return;
		}
		
		int index = users.indexOf(usersFiltered.get(0));
		if (index != -1) {
			users.set(index, u);
			saveToFile();
		}
	}

	public void remove(T u) {
		List<User> usersFiltered = users.stream().filter(user -> user.getId().equals(u.getId())).collect(Collectors.toList());
		
		if(usersFiltered.isEmpty()) {
			return;
		}
		
		int index = users.indexOf(usersFiltered.get(0));
		if (index != -1) {
			users.remove(index);
			saveToFile();
		}
	}

	public List<T> getAll() {
		return users;
	}

	private List<T> readFromFile() {
		T user = null;
		try (FileInputStream fis = new FileInputStream(file);
				ObjectInputStream inputFile = new ObjectInputStream(fis)) {

			while (fis.available() > 0) {
				user = (T) inputFile.readObject();
				users.add(user);
			}
		} catch (Exception e) {
			System.out.println("ERRO ao ler usuario no disco!");
			e.printStackTrace();
		}
		return users;
	}

	private void saveToFile() {
		try {
			close();
			fos = new FileOutputStream(file, false);
			outputFile = new ObjectOutputStream(fos);

			for (T user : users) {
				outputFile.writeObject(user);
			}
			outputFile.flush();
		} catch (Exception e) {
			System.out.println("ERRO ao gravar usuario no disco!");
			e.printStackTrace();
		}
	}

	private void close() throws IOException {
		if (outputFile != null ) {
			outputFile.close();
			fos.close();
			outputFile = null;
			fos = null;
		}
	}
	
	private Long getNextId() {
		if(users == null) {
			return 0L;
		} else if(users.isEmpty()) {
			return 1L;
		}
		
		return users.get(users.size() - 1).getId() + 1;
	}
	
	public boolean emailExists(final String email) {
		return !users.stream().filter(u -> u.getEmail().equals(email)).collect(Collectors.toList()).isEmpty();
	}
	
	public ResponseLoginDTO login(final String email, final String password) {
		
		ResponseLoginDTO dto = new ResponseLoginDTO();
		
		users.stream().filter(u -> u.getEmail().equals(email) && u.getPassword().equals(password)).forEach(u -> {
			dto.setAuth(true);
			dto.setUser(u);
			if(u instanceof Organizer) {
				dto.setUserType(UserTypeEnum.ORGANIZER);
			} else if (u instanceof Exhibitor) {
				dto.setUserType(UserTypeEnum.EXHIBITOR);
			}
		});
		
		return dto;
	}

	@Override
	public void finalize() throws Throwable {
		this.close();
	}

}
