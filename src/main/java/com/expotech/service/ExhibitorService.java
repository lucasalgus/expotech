package com.expotech.service;

import java.io.IOException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.expotech.base.Exhibitor;
import com.expotech.dao.UserDAO;

@Path("exhibitor")
public class ExhibitorService {
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Exhibitor getExhibitor(@PathParam("id") Long id) {
		try {
			return new UserDAO<Exhibitor>("user.bin").get(id);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addExhibitor(Exhibitor user) {
		UserDAO<Exhibitor> dao;
		try {
			dao = new UserDAO<Exhibitor>("user.bin");
			
			if(dao.emailExists(user.getEmail())) {
				return Response.status(Status.BAD_REQUEST).entity("Já existe um usuário com esse email.").build();
			}
			
			dao.add(user);
			return Response.status(Status.CREATED).build();
		} catch (IOException e2) {
			e2.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateExhibitor(Exhibitor user) {
		try {
			new UserDAO<Exhibitor>("user.bin").update(user);
			return Response.ok().build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
