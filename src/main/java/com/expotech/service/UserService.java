package com.expotech.service;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.expotech.base.User;
import com.expotech.dao.UserDAO;
import com.expotech.dto.LoginDTO;
import com.expotech.dto.ResponseLoginDTO;

@Path("user")
public class UserService {

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getAllUsers() {
		try {
			return new UserDAO("user.bin").getAll();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@PathParam("id") Long id) {
		try {
			return new UserDAO("user.bin").get(id);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUser(User user) {
		UserDAO dao;
		try {
			dao = new UserDAO("user.bin");
			
			if(dao.emailExists(user.getEmail())) {
				return Response.status(Status.BAD_REQUEST).entity("Já existe um usuário com esse email.").build();
			}
			
			dao.add(user);
			return Response.status(Status.CREATED).build();
		} catch (IOException e2) {
			e2.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		
	}

	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(User user) {
		try {
			new UserDAO("user.bin").update(user);
			return Response.ok().build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteUser(@PathParam("id") Long id) {
		UserDAO dao;
		try {
			dao = new UserDAO("user.bin");
			User user = dao.get(id);
			dao.remove(user);
			return Response.status(202).entity("Usuário " + id + " removido com sucesso.").build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponseLoginDTO loginOrganizer(LoginDTO user) {
		UserDAO dao;
		ResponseLoginDTO dto = null;
		try {
			dao = new UserDAO("user.bin");
			dto = dao.login(user.getEmail(), user.getPassword());
		} catch (IOException e2) {
			e2.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dto;
		
	}
}
