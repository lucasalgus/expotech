package com.expotech.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.expotech.base.Event;
import com.expotech.base.Exhibitor;
import com.expotech.base.Organizer;
import com.expotech.dao.EventDAO;
import com.expotech.dao.UserDAO;
import com.expotech.dto.EventDTO;
import com.expotech.dto.EventRegistrationDTO;
import com.expotech.enums.TypeOccupationArea;

@Path("event")
public class EventService {

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Event> getAllEvents() {
		try {
			return new EventDAO("event.bin").getAll();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@GET
	@Path("/allFromOrganizer")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Event> getEventsFromOrganizer(@QueryParam("id") int organizerId) {

		try {
			return new EventDAO("event.bin").getAll(organizerId);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@GET
	@Path("/allBy")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Event> getEventsByType(@QueryParam("type") TypeOccupationArea type, @QueryParam("date") Date date ) {

		try {
			return new EventDAO("event.bin").getAll(type);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Event getEvent(@PathParam("id") Long id) {
		try {
			return new EventDAO("event.bin").get(id);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addEvent(EventDTO dto) {
		EventDAO dao;
		try {
			dao = new EventDAO("event.bin");
			
			if(dao.titleExists(dto.getTitle())) {
				return Response.status(Status.BAD_REQUEST).entity("Já existe um evento com esse titulo.").build();
			}
			
			Event event = new Event();
			event.setTitle(dto.getTitle());
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			event.setStartDate(format.parse(dto.getStartDate()));
			event.setEndDate(format.parse(dto.getEndDate()));
			
			event.setDimension(dto.getDimension());
			event.setDescription(dto.getDescription());
			event.setBooths(dto.getBooths());
			
			event.setPhoto(dto.getPhoto());
			event.setColor(dto.getColor());
			event.setType(dto.getType());
			
			UserDAO<Organizer> userDAO = new UserDAO<Organizer>("user.bin");
			event.setOrganizer(userDAO.get(dto.getOrganizerId()));
			
			dao.add(event);
			return Response.status(Status.CREATED).build();
		} catch (IOException | ParseException e2) {
			e2.printStackTrace();
		}

		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		
	}
	
	@POST
	@Path("/addExhibitor")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addExhibitor(EventRegistrationDTO dto) {
		EventDAO dao;
		UserDAO<Exhibitor> userDAO;
		try {
			dao = new EventDAO("event.bin");
			userDAO = new UserDAO<Exhibitor>("user.bin");

			Event event = dao.get(dto.getIdEvent());
			Exhibitor exhibitor = userDAO.get(dto.getIdExhibitor());
			event.addExhibitor(exhibitor);
			
			event.getBooths().stream().filter(b -> b.getId().equals(dto.getIdBooth())).forEach(b -> b.setExhibitor(exhibitor));
			
			dao.update(event);
			
			return Response.status(Status.OK).build();
		} catch (IOException e2) {
			e2.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateEvent(Event event) {
		try {
			new EventDAO("event.bin").update(event);
			return Response.ok().build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteEvent(@PathParam("id") Long id) {
		EventDAO dao;
		try {
			dao = new EventDAO("event.bin");
			Event event = dao.get(id);
			dao.remove(event);
			return Response.status(202).entity("Evento " + id + " removido com sucesso.").build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
