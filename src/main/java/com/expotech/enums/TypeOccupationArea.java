package com.expotech.enums;

public enum TypeOccupationArea {
	PROGRAMMING_TEACHER, DEV_OPS, BACKEND, FRONTEND;
}
