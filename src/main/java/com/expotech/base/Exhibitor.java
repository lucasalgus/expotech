package com.expotech.base;

import java.io.Serializable;
import java.util.List;
import com.expotech.enums.TypeOccupationArea;

public class Exhibitor extends User implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<TypeOccupationArea> listTypes;
	private Portfolio portfolio;
	
	public Exhibitor() {}

	public Exhibitor(String name, String email, String password, String document, String avatar,
			List<TypeOccupationArea> listTypes, Portfolio portfolio) {
		super(name, email, password, document, avatar);
		this.listTypes = listTypes;
		this.portfolio = portfolio;
	}

	public Portfolio getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(Portfolio portfolio) {
		this.portfolio = portfolio;
	}

	public List<TypeOccupationArea> getListTypes() {
		return listTypes;
	}

	public void setListTypes(List<TypeOccupationArea> listTypes) {
		this.listTypes = listTypes;
	}
}