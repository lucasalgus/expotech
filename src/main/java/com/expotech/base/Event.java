package com.expotech.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.expotech.enums.TypeOccupationArea;

public class Event implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String title;
	private String description;
	private Date startDate;
	private Date endDate;
	private Dimension dimension;
	private List<Booth> booths = new ArrayList<Booth>();
	private Organizer organizer;
	private List<Exhibitor> exhibitors = new ArrayList<Exhibitor>();
	private String photo;
	private String color;
	private TypeOccupationArea type;
	
	public Event() {}
	
	public Event(String title, String description, Date startDate, Date endDate, Dimension dimension,
			List<Booth> booths, Organizer organizer, List<Exhibitor> exhibitors, String photo, String color, TypeOccupationArea type) {
		super();
		setTitle(title);
		setDescription(description);
		setStartDate(startDate);
		setEndDate(endDate);
		setDimension(dimension);
		setBooths(booths);
		setOrganizer(organizer);
		setExhibitors(exhibitors);
		setPhoto(photo);
		setColor(color);
		setType(type);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Dimension getDimension() {
		return dimension;
	}
	
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}
	
	public List<Booth> getBooths() {
		return booths;
	}
	
	public void setBooths(List<Booth> booths) {
		this.booths = booths;
	}

	public Organizer getOrganizer() {
		return organizer;
	}

	public void setOrganizer(Organizer organizer) {
		this.organizer = organizer;
	}

	public List<Exhibitor> getExhibitors() {
		return exhibitors;
	}

	public void setExhibitors(List<Exhibitor> exhibitors) {
		this.exhibitors = exhibitors;
	}
	
	public void addExhibitor(Exhibitor exhibitor) {
		exhibitors.add(exhibitor);
	}
	
	public void addBooth(Booth booth) {
		booths.add(booth);
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public TypeOccupationArea getType() {
		return type;
	}

	public void setType(TypeOccupationArea type) {
		this.type = type;
	}
}