package com.expotech.base;

import java.io.Serializable;

public class Organizer extends User implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String site;
	
	public Organizer(String name, String email, String password, String document, String avatar, String site) {
		super(name, email, password, document, avatar);
		this.site = site;
	}

	public Organizer() {}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}
}