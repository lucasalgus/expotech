package com.expotech.base;

import java.io.Serializable;

public class Booth implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private double price;
	private Dimension dimension;
	private Exhibitor exhibitor; 
	
	public Booth() {}

	public Booth(double price, Dimension dimension, Exhibitor exhibitor) {
		super();
		setPrice(price);
		setDimension(dimension);
		setExhibitor(exhibitor);
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public Dimension getDimension() {
		return dimension;
	}
	
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Exhibitor getExhibitor() {
		return exhibitor;
	}

	public void setExhibitor(Exhibitor exhibitor) {
		this.exhibitor = exhibitor;
	}
}