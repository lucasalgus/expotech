package com.expotech.base;

import java.io.Serializable;

public class Dimension implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private double height;
	private double width;
	private double length;
	
	public Dimension() {}
	
	public Dimension(double height, double width, double length) {
		super();
		setHeight(height);
		setWidth(width);
		setLength(length);
	}
	
	public double getHeight() {
		return height;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	public double getLength() {
		return length;
	}
	
	public void setLength(double length) {
		this.length = length;
	}
}