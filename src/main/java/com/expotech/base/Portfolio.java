package com.expotech.base;

import java.io.Serializable;
import java.util.List;

public class Portfolio implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<String> listImages;
	
	public Portfolio() {} 
	
	public Portfolio(List<String> listImages) {
		super();
		setListImages(listImages);
	}

	public List<String> getListImages() {
		return listImages;
	}

	public void setListImages(List<String> listImages) {
		this.listImages = listImages;
	}
}