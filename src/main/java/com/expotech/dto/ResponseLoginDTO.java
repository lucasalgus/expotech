package com.expotech.dto;

import com.expotech.base.User;
import com.expotech.enums.UserTypeEnum;

public class ResponseLoginDTO {
	private boolean auth = false;
	private User user;
	private UserTypeEnum userType;
	
	public ResponseLoginDTO() {}
	
	public boolean isAuth() {
		return auth;
	}
	
	public void setAuth(boolean auth) {
		this.auth = auth;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserTypeEnum getUserType() {
		return userType;
	}

	public void setUserType(UserTypeEnum userType) {
		this.userType = userType;
	}
}
