package com.expotech.dto;

public class EventRegistrationDTO {
	private Long idEvent;
	private Long idExhibitor;
	private Long idBooth;
	
	public Long getIdEvent() {
		return idEvent;
	}
	public void setIdEvent(Long idEvent) {
		this.idEvent = idEvent;
	}
	public Long getIdExhibitor() {
		return idExhibitor;
	}
	public void setIdExhibitor(Long idExhibitor) {
		this.idExhibitor = idExhibitor;
	}
	public Long getIdBooth() {
		return idBooth;
	}
	public void setIdBooth(Long idBooth) {
		this.idBooth = idBooth;
	}
	
	
}
