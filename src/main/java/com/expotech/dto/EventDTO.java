package com.expotech.dto;

import java.util.List;
import com.expotech.base.Booth;
import com.expotech.base.Dimension;
import com.expotech.enums.TypeOccupationArea;

public class EventDTO {
	private String title;
	private String description;
	private String startDate;
	private String endDate;
	private Dimension dimension;
	private List<Booth> booths;
	private Long organizerId;
	private String photo;
	private String color;
	private TypeOccupationArea type;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getStartDate() {
		return startDate;
	}
	
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getEndDate() {
		return endDate;
	}
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public Dimension getDimension() {
		return dimension;
	}
	
	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}
	
	public List<Booth> getBooths() {
		return booths;
	}
	
	public void setBooths(List<Booth> booths) {
		this.booths = booths;
	}
	
	public Long getOrganizerId() {
		return organizerId;
	}
	
	public void setOrganizerId(Long organizerId) {
		this.organizerId = organizerId;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public TypeOccupationArea getType() {
		return type;
	}

	public void setType(TypeOccupationArea type) {
		this.type = type;
	}
	
	
}
