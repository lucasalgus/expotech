package unitTests;

import com.expotech.base.Booth;
import com.expotech.base.Dimension;
import com.expotech.base.Event;
import com.expotech.base.Organizer;
import com.expotech.enums.TypeOccupationArea;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class EventTest {

    Event event;
    Date startDate;
    Date endDateFim;
    Dimension dimension;
    List<Booth> booths ;

    @BeforeEach
    public void setUp(){
        startDate = new Date();
        endDateFim = new Date();
        dimension = new Dimension();
        booths = null;
        event = new Event("DevOps Conference","This is the event description", startDate,endDateFim,dimension,booths, new Organizer(), new ArrayList(), "", "",TypeOccupationArea.BACKEND);
    }

    @Test
    public void getTitleTest(){
        assertThat(event.getTitle(), equalTo("DevOps Conference"));
    }

    @Test
    public void getEmptyTitleTest(){
        assertThat(event.getTitle(), not(isEmptyOrNullString()));
    }

    @Test
    public void getDescriptionTest(){
        assertThat(event.getDescription(), equalTo("This is the event description"));
    }

    @Test
    public void getEmptyDescriptionTest(){
        assertThat(event.getDescription(), not(isEmptyOrNullString()));
    }

    @Test
    public void getStartDateTest(){
        assertThat(event.getStartDate(), equalTo(startDate));
    }

    @Test
    public void getEndDateTest(){
        assertThat(event.getEndDate(), equalTo(endDateFim));
    }

    @Test
    public void getDimensionTest(){
        assertThat(event.getDimension(), equalTo(dimension));
    }

    @Test
    public void getBoothTest(){
        assertThat(event.getBooths(), equalTo(booths));
    }
}
