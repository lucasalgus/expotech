package unitTests;

import com.expotech.base.Organizer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import com.expotech.base.User;


public class OrganizerTest {

    Organizer organizer;

    @BeforeEach
    public void setUp(){
        organizer = new Organizer("","","","","","www.oi.com.br");
    }

    @Test
    public void getSiteTest(){
        assertThat(organizer.getSite(), equalTo("www.oi.com.br"));
    }

    @Test
    public void getEmptySiteTest(){
        assertThat(organizer.getSite(), not(isEmptyOrNullString()));
    }
}
