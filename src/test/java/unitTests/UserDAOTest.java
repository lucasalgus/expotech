package unitTests;

import com.expotech.dao.UserDAO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import com.expotech.base.User;

import java.io.IOException;
import java.util.List;

public class UserDAOTest {

    UserDAO userDAO;
    User user;
    User user2;
    List<User> users;


    @BeforeEach
    public void setUp() throws IOException {
        userDAO = new UserDAO("User");
        user = new User("Samuel", "samuel@gmail.com", "123456", "30030030030", "https//:avatar.com");
        user2 = new User("Arthur", "arthur@gmail.com", "123456", "30030030030", "https//:avatar.com");
        users = userDAO.getAll();
    }

    @Test
    public void addTest(){
        userDAO.add(user);
        assertThat(user.getName(), equalTo(users.get(1).getName()));
    }

    @Test
    public void getTest(){
        userDAO.add(user);
        userDAO.add(user2);
        assertThat(user2.getName(), equalTo(users.get(2).getName()));
    }

    @Test
    public void removeTest(){
        userDAO.add(user);
        userDAO.add(user2);
        userDAO.remove(user2);
        assertThat(users.indexOf(user2), equalTo(-1));
    }

    @Test
    public void updateTest(){
        userDAO.add(user);
        userDAO.add(user2);
        user2.setName("Isabella");
        userDAO.update(user2);
        assertThat(user.getName(), equalTo(users.get(1).getName()));
    }

    @AfterEach
    public void tearDown() throws Throwable {
        users.remove(user);
        users.remove(user2);
        userDAO.finalize();
    }
}
