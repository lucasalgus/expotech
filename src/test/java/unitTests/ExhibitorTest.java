package unitTests;

import com.expotech.base.Portfolio;
import com.expotech.enums.TypeOccupationArea;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import com.expotech.base.Exhibitor;

import java.util.List;

public class ExhibitorTest {

    Exhibitor exhibitor;
    Portfolio portfolio;
    List<TypeOccupationArea> list;
    List<String> images;

    @BeforeEach
    public void setUp(){
        list = null;
        images = null;
        portfolio = new Portfolio(images);
        exhibitor = new Exhibitor("", "", "", "", "", list,portfolio);
    }

    @Test
    public void getListTypesTest(){
        assertThat(exhibitor.getListTypes(), equalTo(list));
    }

    @Test
    public void getPortfolioTest(){
        assertThat(exhibitor.getPortfolio(), equalTo(portfolio));
    }
}
