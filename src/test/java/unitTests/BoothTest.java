package unitTests;

import com.expotech.base.*;
import com.expotech.enums.TypeOccupationArea;
import com.expotech.base.Booth;
import com.expotech.base.Dimension;
import com.expotech.base.Event;
import com.expotech.base.Exhibitor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class BoothTest {

    Booth booth;
    Dimension dimension;
    Exhibitor exhibitor;
    Portfolio portfolio;
    List<TypeOccupationArea> list;
    List<String> images;

    @BeforeEach
    public void setUp(){
        list = null;
        images = null;
        portfolio = new Portfolio(images);
        exhibitor = new Exhibitor("", "", "", "", "", list,portfolio);
        dimension = new Dimension();
        booth = new Booth(200.0,dimension, new Exhibitor());
    }

    @Test
    public void getPriceTest(){
        assertThat(booth.getPrice(), equalTo(200.0));
    }

    @Test
    public void getNullPriceTest(){
        assertThat(booth.getPrice(),notNullValue());
    }

    @Test
    public void getDimensionTest(){
        assertThat(booth.getDimension(),equalTo(dimension));
    }
}
