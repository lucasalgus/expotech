package unitTests;

import com.expotech.base.Booth;
import com.expotech.base.Dimension;
import com.expotech.base.Event;
import com.expotech.base.Organizer;
import com.expotech.dao.EventDAO;
import com.expotech.enums.TypeOccupationArea;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class EventDAOTest {


    EventDAO eventDAO;
    Event event;
    Event event2;
    List<Event> events;
    Date startDate;
    Date endDateFim;
    Dimension dimension;
    List<Booth> booths;


    @BeforeEach
    public void setUp() throws IOException {
        eventDAO = new EventDAO("event");
        startDate = new Date();
        endDateFim = new Date();
        dimension = new Dimension();
        booths = null;
        event = new Event("DevOps Conference","This is the event description", startDate,endDateFim,dimension,booths, new Organizer(), new ArrayList(), "", "",TypeOccupationArea.BACKEND);
        event2 = new Event("QA Conference","This is the event description", startDate,endDateFim,dimension,booths, new Organizer(), new ArrayList(), "", "",TypeOccupationArea.BACKEND);
        events = eventDAO.getAll();
    }

    @Test
    public void addTest(){
        eventDAO.add(event);
        assertThat(event.getTitle(), equalTo(events.get(1).getTitle()));
    }

    @Test
    public void getTest(){
        eventDAO.add(event);
        eventDAO.add(event2);
        assertThat(event2.getTitle(), equalTo(events.get(2).getTitle()));
    }

    @Test
    public void removeTest(){
        eventDAO.add(event);
        eventDAO.add(event2);
        eventDAO.remove(event2);
        assertThat(events.indexOf(event2), equalTo(-1));
    }

    @Test
    public void updateTest(){
        eventDAO.add(event);
        eventDAO.add(event2);
        event2.setTitle("Front-End Conference");
        eventDAO.update(event2);
        assertThat(event.getTitle(), equalTo(events.get(1).getTitle()));
    }

    @AfterEach
    public void tearDown() throws Throwable {
        events.remove(event);
        events.remove(event2);
        eventDAO.finalize();
    }

}
