package unitTests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import com.expotech.base.User;


public class UserTest {

    User user;

    @BeforeEach
    public void setUp(){
        user = new User("Samuel", "samuel@gmail.com", "123456", "30030030030", "https//:avatar.com");
    }

    @Test
    public void getDocumentTest(){
        assertThat(user.getDocument(), equalTo("30030030030"));
    }

    @Test
    public void getEmptyDocumentTest(){
        assertThat(user.getDocument(), not(isEmptyOrNullString()));
    }

    @Test
    public void getEmailTest(){
        assertThat(user.getEmail(), equalTo("samuel@gmail.com"));
    }

    @Test
    public void getEmptyEmailTest(){
        assertThat(user.getEmail(), not(isEmptyOrNullString()));
    }

    @Test
    public void getNameTest(){
        assertThat(user.getName(), equalTo("Samuel"));
    }

    @Test
    public void getEmptyNameTest(){
        assertThat(user.getName(), not(isEmptyOrNullString()));
    }

    @Test
    public void getPasswordTest(){
        assertThat(user.getPassword(), equalTo("123456"));
    }

    @Test
    public void getEmptyPasswordTest(){
        assertThat(user.getPassword(), not(isEmptyOrNullString()));
    }
}
