package unitTests;

import com.expotech.base.Dimension;
import com.expotech.base.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class DimensionTest {

    Dimension dimension;

    @BeforeEach
    public void setUp() {
        dimension = new Dimension(100.0, 100.0, 100.0);
    }

    @Test
    public void getHeightTest() {
        assertThat(dimension.getHeight(), equalTo(100.0));
    }

    @Test
    public void getNullHeightTest() {
        assertThat(dimension.getHeight(), notNullValue());
    }

    @Test
    public void getWidthTest() {
        assertThat(dimension.getWidth(), equalTo(100.0));
    }

    @Test
    public void getNullWidthTest() {
        assertThat(dimension.getWidth(), notNullValue());
    }

    @Test
    public void getLenghtTest() {
        assertThat(dimension.getLength(), equalTo(100.0));
    }

    @Test
    public void getNullLenghtTest() {
        assertThat(dimension.getLength(), notNullValue());
    }
}
