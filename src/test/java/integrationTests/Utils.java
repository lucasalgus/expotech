package integrationTests;

import org.json.JSONObject;

import java.util.Scanner;

public class  Utils {

    private final static String host = "http://localhost:8080/api/";

    public static JSONObject getJSONData(String filePath)
    {
        return new JSONObject(new Scanner(Utils.class.getClassLoader().getResourceAsStream(filePath)).useDelimiter("\\Z").next());
    }

    public static String getHost() {
        return host;
    }
}
