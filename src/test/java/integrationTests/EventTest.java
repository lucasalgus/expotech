package integrationTests;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class EventTest {

    private static final String TESTDATAFILEPATH = "templates/userdata.json";

    private String host = Utils.getHost();

    @BeforeEach
    public void setUp() {
        host +=  "event";
    }

    @Test
    public void eventGetAllTest() {
        given().when().get(host + "/all").then().statusCode(200);
    }

    @Test
    public void eventGetTest() {
        given().when().get(host + "/1").then().statusCode(200);
    }

    @Test
    public void eventGetTypeProgrammingTeacherTest() {
        given().when().get(host + "/all/?type=PROGRAMMING_TEACHER").then().statusCode(200);
    }

    @Test
    public void eventGetTypeFrontEndTest() {
        given().when().get(host + "/all/?type=FRONTEND").then().statusCode(200);
    }

    @Test
    public void eventGetTypeBackEndTest() {
        given().when().get(host + "/all/?type=BACKEND").then().statusCode(200);
    }

    @Test
    public void eventGetTypeDevOpsTest() {
        given().when().get(host + "/all/?type=DEV_OPS").then().statusCode(200);
    }

    @Test
    public void addEventTest() {
        given().contentType(ContentType.JSON).body("{\"idEvent\":5,\"idExhibitor\":3,\"idBooth\":0}").when().post(host + "/addExhibitor").then().statusCode(200);
    }

}