package integrationTests;

import io.restassured.http.ContentType;
import io.restassured.specification.Argument;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.List;

import static io.restassured.RestAssured.given;

public class UserTest {

    private static final String TESTDATAFILEPATH = "../templates/userdata.json";

    private String host = Utils.getHost();

    @BeforeEach
    public void setUpTest() {
        host +=  "user";
    }

    @Test
    public void userGetAllTestTest() {
        given().when().get(host + "/all").then().statusCode(200);
    }

    @Test
    public void userGetTestTest() {
        given().when().get(host + "/1").then().statusCode(200);
    }

    @Test
    public void userLoginPostTest() {
        given().contentType(ContentType.JSON).body("{\"email\":\"a@a.com\",\"password\":\"123\"}").when().post(host + "/login").then().statusCode(200)
                .and().body("auth", equalTo(true));
    }


}